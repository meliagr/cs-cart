<?php

use Tygh\Registry;

if(!defined('BOOTSTRAP')) { die('Access denied'); }

if( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
	if( $mode == "get_email" )
	{
		if (!preg_match('/\A[0-9]+\z/', $_REQUEST["id"])) exit();
		$id = $_REQUEST["id"];
		$db_result = db_get_row("SELECT ?:staff.email AS staff_email, ?:users.email AS user_email FROM ?:staff LEFT JOIN ?:users ON ?:staff.user_id = ?:users.user_id WHERE ?:staff.staff_id = ?i", $id);
		$email = ($db_result["staff_email"] == '' && !is_null($db_result["user_email"])) ? $db_result["user_email"] : $db_result["staff_email"];
		Registry::get('ajax')->assign('email', $email);
		exit;
	}
}
