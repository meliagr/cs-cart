<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
	fn_trusted_vars('staff_data');
	$suffix = '';
	
	if ($mode == 'update')
	{
		//fn_print_die($_REQUEST);
		$staff_id = fn_staff_update_staff_member($_REQUEST["staff_data"], $_REQUEST["staff_id"]);
		$suffix = ".update?staff_id=$staff_id";
	}
	else if ($mode == 'delete')
	{
		if( !empty($_REQUEST["staff_ids"]) )
			fn_staff_remove_by_ids($_REQUEST["staff_ids"]);
		$suffix = '.manage';
	}

    return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);
}

if ($mode == 'manage') 
{
	$arStaff = fn_get_admin_panel_staff_list();
	
	$view = Tygh\Registry::get('view');
	$view->assign('staff_list', $arStaff);
}
else if ($mode == 'update') 
{
	if (!preg_match('/\A[0-9]+\z/', $_REQUEST["staff_id"])) return array(CONTROLLER_STATUS_NO_PAGE);
	$staff = fn_get_staff_member($_REQUEST["staff_id"]);
    if (empty($staff)) return array(CONTROLLER_STATUS_NO_PAGE);
    Tygh::$app['view']->assign('staff', $staff);
}