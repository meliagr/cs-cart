<?php

$schema["staff"] = array(
   'templates' => array (
        'addons/staff_list/blocks/main.tpl' => array(
			'settings' => array (
				'outside_navigation' => array(
					'type' => 'checkbox',
					'default_value' => 'Y'
				),
				'not_scroll_automatically' => array(
					'type' => 'checkbox',
					'default_value' => 'Y'
				),
				'item_quantity' => array (
					'type' => 'input',
					'default_value' => '5'
				),
			),
		)
	),
	'wrappers' => 'blocks/wrappers',
	'content' => array(
		'staff_list' => array(
			'type' => 'function',
			'function' => array('fn_get_staff_list'),
		)
	),
);

return $schema;