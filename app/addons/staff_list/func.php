<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_staff_list()
{
	$join = db_quote(' LEFT JOIN ?:users ON ?:staff.user_id = ?:users.user_id');
	$fields = array(
		'?:staff.staff_id',
		'?:staff.first_name',
		'?:staff.last_name',
		'?:staff.function',
		'?:staff.priority',
		'?:staff.photo',
		'?:staff.user_id',
		'?:users.firstname',
		'?:users.lastname'
	);
	$fields = implode(',', $fields);
	$db_result = db_get_array("SELECT ?p FROM ?:staff ?p ORDER BY ?:staff.priority DESC", $fields, $join);

	$result = array();
	foreach( $db_result as $person )
	{
		if ($person["photo"] == '0') $image = array();
		else $image = fn_get_image_pairs($person["staff_id"], 'staff', 'M', true, true);
		$person_info = array(
			"staff_id" => $person["staff_id"],
			"function" => $person["function"],
			"priority" => $person["priority"],
			"photo" => $image,
		);
		if ($person["user_id"] != '0')
		{
			$first_name = ( $person["first_name"] == '' ) ? $person["firstname"] : $person["first_name"];
			$last_name = ( $person["last_name"] == '' ) ? $person["lastname"] : $person["last_name"];
			$person_info["full_name"] = $first_name.' '.$last_name;
		}
		else
		{
			$person_info["full_name"] = $person["first_name"].' '.$person["last_name"];
		}
		$result[] = $person_info;
	}
	
	return $result;
}

function fn_get_admin_panel_staff_list()
{
	$join = db_quote(' LEFT JOIN ?:users ON ?:staff.user_id = ?:users.user_id');
	$fields = array(
		'?:staff.staff_id',
		'?:staff.first_name',
		'?:staff.last_name',
		'?:staff.function',
		'?:staff.priority',
		'?:staff.email',
		'?:staff.user_id',
		'?:users.firstname',
		'?:users.lastname'
	);
	$fields = implode(',', $fields);
	$db_result = db_get_array("SELECT ?p FROM ?:staff ?p ORDER BY ?:staff.priority DESC", $fields, $join);

	$result = array();
	foreach( $db_result as $person )
	{
		$person_info = array(
			"staff_id" => $person["staff_id"],
			"function" => $person["function"],
			"priority" => $person["priority"],
			"email" => $person["email"],
			"user_id" => (!empty($person["user_id"]) ? $person["user_id"] : ''),
		);
		if ($person["user_id"] != '0')
		{
			$first_name = ( $person["first_name"] == '' ) ? $person["firstname"] : $person["first_name"];
			$last_name = ( $person["last_name"] == '' ) ? $person["lastname"] : $person["last_name"];
			$person_info["full_name"] = $first_name.' '.$last_name;
		}
		else
		{
			$person_info["full_name"] = $person["first_name"].' '.$person["last_name"];
		}
		$result[] = $person_info;
	}
	
	return $result;
}

function fn_get_staff_member($staff_id)
{
	$staff = db_get_row("SELECT * FROM ?:staff WHERE ?:staff.staff_id=?i", $staff_id);
	if ($staff["photo"] == '0') $staff["photo"] = array();
	else 
	{
		$image = fn_get_image_pairs($staff["staff_id"], 'staff', 'M', true, true);
		$staff["photo"] = !empty($image) ? $image : array();
	}
	return $staff;
}

function fn_staff_remove_by_ids($staff_ids)
{
	db_query("DELETE FROM ?:staff WHERE staff_id IN (?n)", $staff_ids);
	foreach ($staff_ids as $id) {
		fn_delete_image_pairs($id, 'staff');
	}
	
	return;
}

function fn_staff_update_staff_member($data, $staff_id)
{
	if($data['user_id'] !== '0')
	{
		$uid = db_get_field("SELECT user_id from ?:users WHERE user_id = ?i", $data['user_id']);
		if($uid === '')
		{
			$data['user_id'] = 0;
			fn_set_notification('E', fn_get_lang_var('error'), __("no_existing_user"), 'I');
		}
	}
	
	if (!empty($staff_id))
	{		
        db_query("UPDATE ?:staff SET ?u WHERE staff_id = ?i", $data, $staff_id);

        $image_needs_update = fn_staff_need_image_update();

        if ($image_needs_update) {
			$pair_data = fn_attach_image_pairs('staff_photo', 'staff', $staff_id);
			if (!empty($pair_data))
			{
				$pair_id = reset($pair_data);
				db_query("UPDATE ?:staff SET photo = ?i WHERE staff_id = ?i", $pair_id, $staff_id);
			}
        }
	}
	else
	{
        $staff_id = db_query("INSERT INTO ?:staff ?e", $data);
        if (fn_staff_need_image_update()) {
			$pair_data = fn_attach_image_pairs('staff_photo', 'staff', $staff_id);
			if (!empty($pair_data))
			{
				$pair_id = reset($pair_data);
				db_query("UPDATE ?:staff SET photo = ?i WHERE staff_id = ?i", $pair_id, $staff_id);
			}
            /*$pair_data = fn_attach_image_pairs('banners_main', 'promo', $banner_image_id, $lang_code);
            if (!empty($pair_data)) {
                $data_banner_image = array(
                    'banner_image_id' => $banner_image_id,
                    'banner_id'       => $banner_id,
                    'lang_code'       => $lang_code
                );

                db_query("INSERT INTO ?:banner_images ?e", $data_banner_image);
                fn_banners_image_all_links($banner_id, $pair_data, $lang_code);
            }*/
        }
	}
	
	return $staff_id;
}

function fn_staff_need_image_update()
{
    if (!empty($_REQUEST['file_staff_photo_image_icon']) && array($_REQUEST['file_staff_photo_image_icon'])) {	// WTF is the second condition? Got it in banners. If somebody understands please reply to odin@null.net
        $image_banner = reset ($_REQUEST['file_staff_photo_image_icon']);

        if ($image_banner == 'staff_photo') {
            return false;
        }
    }

    return true;
}