{** staff_list section **}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="staff_list_form" enctype="multipart/form-data">

{if $staff_list}
	<table class="table table-middle">
	<thead>
	<tr>
		<th width="1%" class="left">
			{include file="common/check_items.tpl" class="cm-no-hide-input"}</th>
		<th>{__("staff.fullname")}</th>
		<th>{__("staff.function")}</th>
		<th>{__("staff.email")}</th>
		<th>{__("staff.user")}</th>
		<th width="7%"></th>
		<th width="7%" class="left">{__("priority")}</th>
	</tr>
	</thead>
		{foreach from=$staff_list item=staff}
		<tr>
			<td class="left">
				<input type="checkbox" name="staff_ids[]" value="{$staff.staff_id}" class="cm-item" />
			</td>
			<td>
				{$staff.full_name}
			</td>
			<td>
				{$staff.function}
			</td>
			<td>
				{$staff.email}
			</td>
			<td>
				{if $staff.user_id != ''}
					<a href="{"profiles.update&user_id=`$staff.user_id`"|fn_url}">{__("staff.user")}</a>
				{/if}
			</td>
			<td>
				<a href="{"staff.update?staff_id=`$staff.staff_id`"|fn_url}">{__("staff.edit")}</a>
			</td>
			<td class="left">
				<a href="{"staff.update?staff_id=`$staff.staff_id`"|fn_url}">{$staff.priority}</a>
			</td>
		</tr>
		{/foreach}
	</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

</form>
{/capture}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $staff_list}
            <li>{btn type="delete_selected" dispatch="dispatch[staff.delete]" form="staff_list_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}

{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff.add" prefix="top" hide_tools="true" title=__("add_staff_member") icon="icon-plus"}
{/capture}

{include file="common/mainbox.tpl" title=__("staff") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=true}

{** ad section **}