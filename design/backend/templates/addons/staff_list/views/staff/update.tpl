{if $staff}
    {assign var="id" value=$staff.staff_id}
{else}
    {assign var="id" value=0}
{/if}

{** staff section **}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit" name="staff_form" enctype="multipart/form-data">
	<input type="hidden" class="cm-no-hide-input" name="staff_id" value="{$id}" />

	<div id="content_general">
		<div class="control-group">
			<label for="elm_staff_first_name" class="control-label">{__("staff.first_name")}</label>
			<div class="controls">
				<input type="text" name="staff_data[first_name]" id="elm_staff_first_name" value="{$staff.first_name}" size="25" class="input-large" />
			</div>
		</div>
		
		<div class="control-group">
			<label for="elm_staff_last_name" class="control-label">{__("staff.last_name")}</label>
			<div class="controls">
				<input type="text" name="staff_data[last_name]" id="elm_staff_last_name" value="{$staff.last_name}" size="25" class="input-large" />
			</div>
		</div>
		
		<div class="control-group">
			<label for="elm_staff_email" class="control-label">{__("staff.email")}</label>
			<div class="controls">
				<input type="text" name="staff_data[email]" id="elm_staff_email" value="{$staff.email}" size="25" class="input-large" />
			</div>
		</div>
		
		<div class="control-group" id="staff_function">
			<label for="elm_staff_function" class="control-label cm-required">{__("staff.function")}</label>
			<div class="controls">
				<textarea id="elm_staff_function" name="staff_data[function]" cols="20" rows="2" class="cm-wysiwyg input-textarea-long">{$staff.function}</textarea>
			</div>
		</div>

		<div class="control-group">
			<label for="elm_staff_user_id" class="control-label">{__("staff.user_id")}</label>
			<div class="controls">
				<input type="text" name="staff_data[user_id]" id="elm_staff_user_id" value="{$staff.user_id|default:"0"}" size="6"/>
			</div>
		</div>

		<div class="control-group">
			<label for="elm_staff_priority" class="control-label">{__("staff.priority")}</label>
			<div class="controls">
				<input type="text" name="staff_data[priority]" id="elm_staff_priority" value="{$staff.priority|default:"0"}" size="3"/>
			</div>
		</div>

		<div class="control-group" id="staff_picture">
			<label class="control-label" class="cm-required">{__("staff.photo")}</label>
			<div class="controls">
				{include file="common/attach_images.tpl" image_name="staff_photo" image_object_type="staff" image_pair=$staff.photo image_object_id=$id no_detailed=true hide_titles=true}
			</div>
		</div>
	</div>

	{capture name="buttons"}
		{if !$id}
			{include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="staff_form" but_name="dispatch[staff.update]"}
		{else}
			{include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" but_role="submit-link" but_target_form="staff_form" save=$id}
		{/if}
	{/capture}
    
</form>

{/capture}

{if !$id}
    {assign var="title" value=__("new_staff_member")}
{else}
    {assign var="title" value="{__("edit_staff_member")}: `$staff.full_name`"}
{/if}
{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}