{** block-description:staff_list_block_description **}

{assign var="obj_prefix" value="`$block.block_id`000"}
<div class="staff-slide">
	<div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
	<div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>

	<div id="scroll_list_{$block.block_id}" class="owl-carousel ty-scroller-list">
		{foreach from=$staff_list item="staff" name="for_staff_list"}
			
			<div class="ty-scroller-list__item">
				{assign var="obj_id" value="scr_`$block.block_id`000`$staff.staff_id`"}
				<div class="ty-scroller-list__img-block">
					{include file="common/image.tpl" assign="object_img" images=$staff.photo image_width=179 image_height=200 no_ids=true lazy_load=true}
					<div>{$object_img nofilter}</div>
				</div>
				<div class="ty-scroller-list__description">
					<span><strong id="id1">{$staff.full_name}</strong></span><br>
					<span>{$staff.function}</span><br>
					<a 
						class="email"
						data-id="{$staff.staff_id}"
						href="{"staff_info.get_email?id=`$staff.staff_id`"|fn_url}"
					>
						Показать e-mail
					</a>
				</div>
			</div>
		{/foreach}
	</div>

	{literal}
	<script>
	$(document).ready(function(){
		$("#{/literal}scroll_list_{$block.block_id}{literal} a.email").click(function(e){
			e.preventDefault();
			e.stopPropagation();
			var id = $(this).attr("data-id"),
				self = this;
			$.ceAjax(
				'request', 
				"{/literal}{"staff_info.get_email"|fn_url}{literal}", 
				{
					method: "POST",
					cache: false,
					data: { 'id' : id },
					callback: function(data) {
						if(data.email !== "undefined" && data.email !== "") {
							var a = document.createElement("a");
							a.href = "mailto:" + data.email;
							a.innerHTML = data.email;
							$(self).parent().append(a);
						}
						$(self).remove();
					}
				}
			);
		});
		/*$.ceEvent('on', 'ce.ajaxdone', function(data){		Why doesn't this event work properly? Where is data?
			console.log(data);
		});*/
	});
	</script>
	{/literal}
</div>

{include file="common/scroller_init.tpl" prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}